﻿/*!
* jQuery Expander Plugin v1.4.3
*
* Date: Tue Jun 19 11:54:00 2012 EDT
* Requires: jQuery v1.3+
*
* Copyright 2011, Karl Swedberg
* Dual licensed under the MIT and GPL licenses (just like jQuery):
* http://www.opensource.org/licenses/mit-license.php
* http://www.gnu.org/licenses/gpl.html
*
*
*
*
*/
(function (c) {
    c.expander = { version: "1.4.3", defaults: { slicePoint: 100, preserveWords: true, widow: 4, expandText: "read more", expandPrefix: "&hellip; ", expandAfterSummary: false, summaryClass: "summary", detailClass: "details", moreClass: "read-more", lessClass: "read-less", collapseTimer: 0, expandEffect: "slideDown", expandSpeed: 250, collapseEffect: "slideUp", collapseSpeed: 200, userCollapse: true, userCollapseText: "read less", userCollapsePrefix: " ", onSlice: null, beforeExpand: null, afterExpand: null, onCollapse: null} }; c.fn.expander =
function (l) {
    function J(a, d) { var g = "span", h = a.summary; if (d) { g = "div"; if (x.test(h) && !a.expandAfterSummary) h = h.replace(x, a.moreLabel + "$1"); else h += a.moreLabel; h = '<div class="' + a.summaryClass + '">' + h + "</div>" } else h += a.moreLabel; return [h, "<", g + ' class="' + a.detailClass + '"', ">", a.details, "</" + g + ">"].join("") } function K(a) { var d = '<span class="' + a.moreClass + '">' + a.expandPrefix; d += '<a href="#">' + a.expandText + "</a></span>"; return d } function y(a, d) {
        if (a.lastIndexOf("<") > a.lastIndexOf(">")) a = a.slice(0, a.lastIndexOf("<"));
        if (d) a = a.replace(L, ""); return c.trim(a)
    } function z(a, d) { d.stop(true, true)[a.collapseEffect](a.collapseSpeed, function () { d.prev("span." + a.moreClass).show().length || d.parent().children("div." + a.summaryClass).show().find("span." + a.moreClass).show() }) } function M(a, d, g) { if (a.collapseTimer) A = setTimeout(function () { z(a, d); c.isFunction(a.onCollapse) && a.onCollapse.call(g, false) }, a.collapseTimer) } var v = "init"; if (typeof l == "string") { v = l; l = {} } var s = c.extend({}, c.expander.defaults, l), N = /^<(?:area|br|col|embed|hr|img|input|link|meta|param).*>$/i,
L = s.wordEnd || /(&(?:[^;]+;)?|[a-zA-Z\u00C0-\u0100]+)$/, B = /<\/?(\w+)[^>]*>/g, C = /<(\w+)[^>]*>/g, D = /<\/(\w+)>/g, x = /(<\/[^>]+>)\s*$/, O = /^<[^>]+>.?/, A; l = { init: function () {
    this.each(function () {
        var a, d, g, h, m, i, o, w, E = [], t = [], p = {}, q = this, e = c(this), F = c([]), b = c.extend({}, s, e.data("expander") || c.meta && e.data() || {}); i = !!e.find("." + b.detailClass).length; var r = !!e.find("*").filter(function () { return /^block|table|list/.test(c(this).css("display")) }).length, u = (r ? "div" : "span") + "." + b.detailClass, G = "span." + b.moreClass,
P = b.expandSpeed || 0, n = c.trim(e.html()); c.trim(e.text()); var f = n.slice(0, b.slicePoint); if (!c.data(this, "expanderInit")) {
            c.data(this, "expanderInit", true); c.each(["onSlice", "beforeExpand", "afterExpand", "onCollapse"], function (j, k) { p[k] = c.isFunction(b[k]) }); f = y(f); for (d = f.replace(B, "").length; d < b.slicePoint; ) { a = n.charAt(f.length); if (a == "<") a = n.slice(f.length).match(O)[0]; f += a; d++ } f = y(f, b.preserveWords); h = f.match(C) || []; m = f.match(D) || []; g = []; c.each(h, function (j, k) { N.test(k) || g.push(k) }); h = g; d = m.length; for (a =
0; a < d; a++) m[a] = m[a].replace(D, "$1"); c.each(h, function (j, k) { var H = k.replace(C, "$1"), I = c.inArray(H, m); if (I === -1) { E.push(k); t.push("</" + H + ">") } else m.splice(I, 1) }); t.reverse(); if (i) { i = e.find(u).remove().html(); f = e.html(); n = f + i; a = "" } else { i = n.slice(f.length); a = c.trim(i.replace(B, "")); if (a === "" || a.split(/\s+/).length < b.widow) return; a = t.pop() || ""; f += t.join(""); i = E.join("") + i } b.moreLabel = e.find(G).length ? "" : K(b); if (r) i = n; f += a; b.summary = f; b.details = i; b.lastCloseTag = a; if (p.onSlice) b = (g = b.onSlice.call(q, b)) &&
g.details ? g : b; r = J(b, r); e.html(r); o = e.find(u); w = e.find(G); o[b.collapseEffect](0); w.find("a").unbind("click.expander").bind("click.expander", function (j) { j.preventDefault(); w.hide(); F.hide(); p.beforeExpand && b.beforeExpand.call(q); o.stop(false, true)[b.expandEffect](P, function () { o.css({ zoom: "" }); p.afterExpand && b.afterExpand.call(q); M(b, o, q) }) }); F = e.find("div." + b.summaryClass); b.userCollapse && !e.find("span." + b.lessClass).length && e.find(u).append('<span class="' + b.lessClass + '">' + b.userCollapsePrefix + '<a href="#">' +
b.userCollapseText + "</a></span>"); e.find("span." + b.lessClass + " a").unbind("click.expander").bind("click.expander", function (j) { j.preventDefault(); clearTimeout(A); j = c(this).closest(u); z(b, j); p.onCollapse && b.onCollapse.call(q, true) })
        } 
    })
}, destroy: function () {
    if (this.data("expander")) {
        this.removeData("expander"); this.each(function () {
            var a = c(this), d = c.meta ? c.extend({}, s, a.data()) : s, g = a.find("." + d.detailClass).contents(); a.find("." + d.moreClass).remove(); a.find("." + d.summaryClass).remove(); a.find("." + d.detailClass).after(g).remove();
            a.find("." + d.lessClass).remove()
        })
    } 
} 
}; l[v] && l[v].call(this); return this
}; c.fn.expander.defaults = c.expander.defaults
})(jQuery);